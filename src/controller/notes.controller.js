const mysqlconnect = require("./../connections/db");

module.exports = {
  getnotes: async (req, res) => {
    const id = req.query.id;
    const result = await mysqlconnect.query("call spgetnotes(:id)",{
      replacements:{
        id
      }
    });
    res.json(result);
  },
  getonenote: async (req, res) => {
    const id = req.params.id;
    const result = await mysqlconnect.query("call spgetonenote(:id)", {
      replacements: {
        id,
      },
    });
    res.json(result);
  },

  insertnotes: async (req, res) => {
    const { title, description,id } = req.body;

    const result = await mysqlconnect.query(
      "call spinsertnotes(:title,:description,:id)",
      {
        replacements: {
          title,
          description,
          id
        },
      }
    );
    res.json(result[0]);
  },
  deletenotes: async (req, res) => {
    const noteid = req.query.noteid;
    const result = await mysqlconnect.query("call spdeletenotes(:noteid)", {
      replacements: {
        noteid,
      },
    });
    res.json(result[0]);
  },
  updatenotes: async (req, res) => {
    const { id, title, description } = req.body;

    const result = await mysqlconnect.query(
      "call spupdatenotes(:id,:title,:description)",
      {
        replacements: {
          id,
          title,
          description,
        },
      }
    );
    res.json(result[0]);
  },
  login: async (req, res) => {
    const { user, pass } = req.body;

    const result = await mysqlconnect.query("call splogin(:user,:pass)", {
      replacements: {
        user,
        pass,
      },
    }); 
    res.json(result[0]);
  },
};
