const express = require('express');
const notesController = require('../controller/notes.controller');

const router = express.Router();

router.get('/',notesController.getnotes)
router.get('/:id',notesController.getonenote)
router.post('/',notesController.insertnotes)
router.post('/auth',notesController.login)
router.put('/',notesController.updatenotes)
router.delete('/',notesController.deletenotes)

module.exports=router;