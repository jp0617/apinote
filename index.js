const express=require('express')

const notesroutes=require('./src/routes/notes.routes')
const cors=require('cors')
const app=express()

app.set('port',3000)
app.use(express.json())
app.use(cors())

app.use('/',notesroutes)

app.listen(app.get('port'),()=>{
    console.log('listening on port ',app.get('port'));
})